package paystation.domain;

/**
 * Created by nathan.gingrich on 9/27/2016.
 */
public class USDAcceptanceStrategy implements AcceptanceStrategy {
    @Override
    public boolean isValidCoin(int coin) throws IllegalCoinException {
        switch (coin) {
            case 5:
            case 10:
            case 25:
                return true;
            default:
                throw new IllegalCoinException("Invalid coin input.");
        }
    }
}
