package paystation.domain;

/**
 * A linear calculation rate strategy.
 * <p>
 * Responsibilities:
 * <p>
 * 1) Calculate rates so each 5 cent gives 2 minutes parking
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Computer Science Department
 * Aarhus University
 * <p>
 * This source code is provided WITHOUT ANY WARRANTY either
 * expressed or implied. You may study, use, modify, and
 * distribute it for non-commercial purposes. For any
 * commercial use, see http://www.baerbak.com/
 */
public class LinearRateStrategy implements RateStrategy {
    private double rate;

    public LinearRateStrategy(double rate) {
        this.rate = rate;
    }

    public int calculateTime(int amount) {
        return (int)(amount * rate);
    }
}

