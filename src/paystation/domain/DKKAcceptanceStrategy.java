package paystation.domain;

/**
 * Created by nathan.gingrich on 9/27/2016.
 */
public class DKKAcceptanceStrategy implements AcceptanceStrategy {

    @Override
    public boolean isValidCoin(int coin) throws IllegalCoinException {
        switch(coin) {
            case 1:
            case 2:
            case 5:
            case 10:
            case 20:
                return true;
            default:
                throw new IllegalCoinException("Invalid kroner input");
        }
    }
}
