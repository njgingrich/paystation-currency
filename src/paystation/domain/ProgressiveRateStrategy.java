package paystation.domain;

/**
 * A progressive calculation rate strategy.
 * <p>
 * Responsibilities:
 * <p>
 * 1) Calculate progressive rates so
 * First hour costs 150 cents per hour;
 * Second hour costs 200 cents per hour;
 * Third and following hours cost 300 cents per hour.
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Computer Science Department
 * Aarhus University
 * <p>
 * This source code is provided WITHOUT ANY WARRANTY either
 * expressed or implied. You may study, use, modify, and
 * distribute it for non-commercial purposes. For any
 * commercial use, see http://www.baerbak.com/
 */
public class ProgressiveRateStrategy implements RateStrategy {
    private double[] rates;
    private int[] times;

    public ProgressiveRateStrategy(double[] rates, int[] times) {
        this.rates = rates;
        this.times = times;
    }

    private int amountForTime(int time, double rate) {
        return (int) (time * (1 / rate));
    }

    public int calculateTime(int amount) {
        int time = 0;
        int firstTimeRate = amountForTime(times[0], rates[0]); // 150
        int secondTimeRate = amountForTime(times[1], rates[1]); // 200

        if (amount <= firstTimeRate) {
            time = (int)(amount * rates[0]);
        } else if (amount <= secondTimeRate) {
            amount -= (firstTimeRate);
            time = (int)(times[0] + amount * rates[1]);
        } else {
            amount -= (firstTimeRate + secondTimeRate);
            time = (int)(times[0] + times[1] + amount * rates[2]);
        }
        return time;
    }
}
