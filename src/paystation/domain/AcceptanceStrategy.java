package paystation.domain;

/**
 * Created by nathan.gingrich on 9/27/2016.
 */
public interface AcceptanceStrategy {

    /***
     * Determine if a coin is a valid coin to input or not.
     *
     * @param coin The coin entered into the pay station.
     * @return True if the coin is valid, false otherwise.
     */
    public boolean isValidCoin(int coin) throws IllegalCoinException;
}
