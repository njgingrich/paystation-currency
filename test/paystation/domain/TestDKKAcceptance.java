package paystation.domain;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by nathan.gingrich on 9/27/2016.
 */
public class TestDKKAcceptance {
    private AcceptanceStrategy as;

    @Before
    public void setUp() {
        as = new DKKAcceptanceStrategy();
    }

    /**
     * All the coins tested here are valid inputs.
     * @throws IllegalCoinException
     */
    @Test
    public void testAllValidCoins() throws IllegalCoinException {
        assertTrue("1 cent is valid input", as.isValidCoin(1));
        assertTrue("2 cents is valid input", as.isValidCoin(2));
        assertTrue("5 cents is valid input", as.isValidCoin(5));
        assertTrue("10 cents is valid input", as.isValidCoin(10));
        assertTrue("20 cents is valid input", as.isValidCoin(20));
    }

    /**
     * This test should throw an IllegalCoinException as the input
     * is not valid.
     * @throws IllegalCoinException
     */
    @Test(expected=IllegalCoinException.class)
    public void shouldRejectIllegalCoin() throws IllegalCoinException {
        assertFalse("12 coins isn't valid input", as.isValidCoin(12));
        assertFalse("21 coins isn't valid input", as.isValidCoin(21));
    }
}
