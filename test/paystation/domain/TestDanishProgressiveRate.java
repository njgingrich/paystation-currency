package paystation.domain;

import org.junit.*;
import org.junit.experimental.theories.suppliers.TestedOn;

import static org.junit.Assert.*;

/**
 * Created by nathan on 9/27/16.
 */
public class TestDanishProgressiveRate {
    RateStrategy rs;

    @Before
    public void setUp() {
        rs = new ProgressiveRateStrategy(
                new double[]{(double)6, (double)5, (double)5}, // ew
                new int[]{120, 120}
        );
    }

    /**
     * Test a single hour of parking
     */
    @Test
    public void shouldGive60MinFor10Kroner() {
        assertEquals(60, rs.calculateTime(10));
    }

    /**
     * Test 2 hours parking
     */
    @Test
    public void shouldGive120MinFor20Kroner() {
        assertEquals(120, rs.calculateTime(20));
    }

    /**
     * Test 3 hours parking
     */
    @Test
    public void shouldGive180MinFor32Kroner() {
        assertEquals(180, rs.calculateTime(32));
    }

}
