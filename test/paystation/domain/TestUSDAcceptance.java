package paystation.domain;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Created by nathan.gingrich on 9/27/2016.
 */
public class TestUSDAcceptance {
    private AcceptanceStrategy as;

    @Before
    public void setUp() {
        as = new USDAcceptanceStrategy();
    }

    /** Test acceptance of all legal coins */
    @Test
    public void shouldAcceptLegalCoins() throws IllegalCoinException {
        // Only valid coins are 5, 10, and 25.
        assertTrue("5 cents is a valid coin input", as.isValidCoin(5));
        assertTrue("10 cents is a valid coin input", as.isValidCoin(10));
        assertTrue("25 cents is a valid coin input", as.isValidCoin(25));
    }

    /**
     * Verify that illegal coin values are rejected.
     */
    @Test(expected=IllegalCoinException.class)
    public void shouldRejectIllegalCoin() throws IllegalCoinException {
        assertFalse("17 cents isn't a valid coin input", as.isValidCoin(17));
    }
}
