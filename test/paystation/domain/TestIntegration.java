package paystation.domain;

import org.junit.*;

import static org.junit.Assert.*;

/**
 * Integration testing of the configurations of the pay station.
 * <p>
 * This source code is from the book
 * "Flexible, Reliable Software:
 * Using Patterns and Agile Development"
 * published 2010 by CRC Press.
 * Author:
 * Henrik B Christensen
 * Computer Science Department
 * Aarhus University
 * <p>
 * This source code is provided WITHOUT ANY WARRANTY either
 * expressed or implied. You may study, use, modify, and
 * distribute it for non-commercial purposes. For any
 * commercial use, see http://www.baerbak.com/
 */
public class TestIntegration {
    private PayStation ps;

    /**
     * Integration testing for the linear rate configuration
     */
    @Test
    public void shouldIntegrateLinearRateCorrectly()
            throws IllegalCoinException {
        // Configure pay station to be the progressive rate pay station
        ps = new PayStationImpl(new LinearRateStrategy(((double) 2) / 5),
                new AnyCoinAcceptanceStrategy());
        // add $ 2.0:
        addOneDollarUSD();
        addOneDollarUSD();

        assertEquals("Linear Rate: 2$ should give 80 min ",
                80, ps.readDisplay());
    }

    /**
     * Integration testing for the progressive rate configuration
     */
    @Test
    public void shouldIntegrateProgressiveRateCorrectly()
            throws IllegalCoinException {
        // reconfigure ps to be the progressive rate pay station
        ps = new PayStationImpl(new ProgressiveRateStrategy(
                    new double[]{((double) 2 / 5), 1.5 / 5, ((double) 1 / 5)},
                    new int[]{60, 60}),
                new AnyCoinAcceptanceStrategy());
        // add $ 2.0: 1.5 gives 1 hours, next 0.5 gives 15 min
        addOneDollarUSD();
        addOneDollarUSD();

        assertEquals("Progressive Rate: 2$ should give 75 min ",
                75, ps.readDisplay());
    }

    /**
     * Integration testing for the USD acceptance strategy
     *
     * @throws IllegalCoinException
     */
    @Test
    public void shouldIntegrateUSDAcceptanceStrategy()
            throws IllegalCoinException {
        ps = new PayStationImpl(new One2OneRateStrategy(),
                new USDAcceptanceStrategy());
        // add $1.00 should give 100 minutes.
        addOneDollarUSD();
        assertEquals("USDAcceptanceStrategy: $1.00 should give 100 min",
                100, ps.readDisplay());
    }

    @Test
    public void shouldIntegrateDKKAcceptanceStrategy()
            throws IllegalCoinException {
        ps = new PayStationImpl(new One2OneRateStrategy(),
                new DKKAcceptanceStrategy());
        // adding 80 kroner should give 80 minutes.
        ps.addPayment(20);
        ps.addPayment(20);
        ps.addPayment(20);
        ps.addPayment(10);
        ps.addPayment(5);
        ps.addPayment(5);
        assertEquals("DKKAcceptanceStrategy: 80kr should give 80 min",
                80, ps.readDisplay());
    }

    private void addOneDollarUSD() throws IllegalCoinException {
        ps.addPayment(25);
        ps.addPayment(25);
        ps.addPayment(25);
        ps.addPayment(25);
    }
}