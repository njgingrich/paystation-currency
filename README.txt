1)
First, I refactored the PayStationImpl to take an AcceptanceStrategy, specifically
a USDAcceptanceStrategy and reran my tests to ensure that Alphatown's PayStation
was still working as expected. I then refactored the coin tests in TestPayStation
into TestUSDAcceptance as those tests are only testing the valid input of US coins.
Since the USDAcceptance tests don't need a PayStation initialized, I refactored
the tests themselves.

Then, to keep the TestPayStation decoupled from testing coin acceptance, I added
a AnyCoinAcceptanceStrategy to the test directory, so any coins inputted into
TestPayStation would be valid.

Next, I added a DKKAcceptanceStrategy, and added a test class for testing
DKK acceptance. Then to check integration, I added tests to TestIntegration
to test how the DKKAcceptanceStrategy interacted with the PayStationImpl.

2)
The tests in TestIntegration are integration tests because they check how
the acceptance strategies work when used in the actual PayStation, instead
of their unit tests which are in TestUSDAcceptance and TestDKKAcceptance for
the USDAcceptanceStrategy and the DKKAcceptanceStrategy.

3)
One pro of the strategy pattern is certainly easier testing by putting a
responsibility outside of the "parent" class (not inheritance, just the owner)
into a delegated new class, which can be tested separately from the PayStation
itself.
One con is more boilerplate code; even though it is easier to read through
the PayStation class, there are several more files, which is better than
the other methods discussed like polymorphism, but just a bit more cluttered.
Overall I definitely like using the strategy pattern.
